<!DOCTYPE html> 

<html>

<head>

<?php
$tempa = "posts/".$_GET["p"];

if (file_exists($tempa)) {

	require_once($tempa);
	echo "<title>Portuguèssimo - ".$p[0]."</title>";

} else {

	echo "<title>Portuguéssimo - 404</title>";
	$nf = true;
}
	
?>
	<meta name="DESCRIPTION" content="Portuguessimo"></meta>
	<meta name="AUTHOR" content="Yotxi"></meta>
	<link href="estil.css" rel="stylesheet" type="text/css" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

</head>



<body>

	<header>
	<div class="h-h">
		<span class="h-h-t">Portuguéssimo</span><br>
		<span class="h-h-h">"Nao se preocupe: seja feliz!"</span>
	</div>

	<div class="h-contain">

		<a href="index.php" class="h-c-a"><div class="h-container">Index</div></a>
		<a href="blog.php?p=a1" class="h-c-a"><div class="h-container">How to use this</div></a>
		<a href="jogos.php" class="h-c-a"><div class="h-container">Games</div></a>
		<a href="outros.php" class="h-c-a"><div class="h-container">Other</div></a>
		<a href="sobre.html" class="h-c-a"><div class="h-container">About</div></a>

	</div>
	</header>

	<div class="filler"/>

	<div class="body">

		<div class="body-box">
<?php

if (!isset($nf)) {

	echo "<div class=\"body-box-title\">\n&nbsp".$p[0]."\n</div>";
	echo "<div class=\"body-box-filler\"></div>\n";
	echo "<div class=\"body-box-content\">\n".$p[1]."\n</div>\n";

	if (substr($_GET["p"],0,1) == "b") {


		if (substr($_GET["p"], 1) < count(glob("posts/b*"))) {
			
			$tempb = substr($_GET["p"], 1);
			$tempb = $tempb + 1;
			require_once("posts/b".$tempb);
			echo "</div>\n<div class=\"body-box\">\n";
			echo "<a href=\"blog.php?p=b".$tempb."\" class=\"body-box-read\">Next: ".$p[0]."</a>\n";

		}

		if (substr($_GET["p"],1) > 1) {
			
			$tempb = substr($_GET["p"],1);
			$tempb--;
			require_once("posts/b".$tempb);
			echo "<a href=\"blog.php?p=b".$tempb."\" class=\"body-box-read\">Previous: ".$p[0]."</a>\n</div>";

		}

	}

} else {

	echo "<div class=\"body-box-title\">¡404 error!</div>";
	
	echo "<div class=\"body-box-content\"> That <b>hasn't been found</b>, man: what a shame. Maybe you would like going to the <a href=\"index.php\">índex</a> and see what else there is on this site.<br>We're sorry. Excuse me. Forgive us. This has just happened and we're still screaming of fear
	<br><br>
	Though, here's a funny dog to make you happy:<br><br>
	<center><img src=\"img/gimp.gif\"></center>
	</div>";

}


?>
		</div>

	</div>

	<footer>

		<div class="f-r">Portuguessimo by <a href="https://gitlab.com/Yotxi/" style="color:white; ">Yotxi</a><br>Under MIT license.<br>Made with love and vim.</div>

	</footer>

</body>

</html>

